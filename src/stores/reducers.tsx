import { configureStore } from "@reduxjs/toolkit";

import authReducer from "./slice/authSlice";
import roomReducer from "./slice/roomSlice";

export default configureStore({
    reducer: {
        auth: authReducer,
        room: roomReducer,
    },
});
