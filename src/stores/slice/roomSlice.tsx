import { createSlice } from '@reduxjs/toolkit';

export const roomSlice = createSlice({
    name: 'auth',
    initialState: {
        avilableRooms: [],
        selectedRoom: {},
        lastBooking: {},
    },
    reducers: {
        setAvilableRooms: (state, action) => {
            state.avilableRooms = action.payload;
        },
        selectedRoom: (state, action) => {
            state.selectedRoom = action.payload;
        },
        setLastBooking: (state, action) => {
            state.lastBooking = action.payload;
        }
    },
});

export const { 
    setAvilableRooms, selectedRoom, setLastBooking
} = roomSlice.actions;

export default roomSlice.reducer;
