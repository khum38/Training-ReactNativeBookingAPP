import { createSlice } from '@reduxjs/toolkit';

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        accessToken: '',
        refreshToken: '',
        userEmail: '',
        userId: '',
    },
    reducers: {
        setAccessToken: (state, action) => {
            state.accessToken = action.payload;
        },
        setRefreshToken: (state, action) => {
            state.refreshToken = action.payload;
        },
        setUserEmail: (state, action) => {
            state.userEmail = action.payload;
        },  
        setUserId: (state, action) => {
            state.userId = action.payload;
        },
        resetData: (state) => {
            state.accessToken = '';
            state.refreshToken = '';
            state.userEmail = '';
            state.userId = '';
        }
    },
});

export const { 
    setAccessToken, 
    setRefreshToken, 
    setUserEmail, 
    setUserId,
    resetData
} = authSlice.actions;

export default authSlice.reducer;
