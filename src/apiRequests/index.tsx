import axios from "axios";
import { Int32 } from "react-native/Libraries/Types/CodegenTypes";

const baseUrl = 'http://localhost:8080';

export const singUpRequest = async (email: String, password: String) => {
    const configurationObject = {
        method: 'post',
        data: {
            email: email,
            password: password,
        },
        url: `${baseUrl}/api/users/authenticate`,
    };
    try {
        const response = await axios(configurationObject);
        return response;
    } catch (error: any) {
        return error.response;
    }
};

export const registerAccountRequest = async (firstName: String, lastName: String, email: String, password: String) => {
    const configurationObject = {
        method: 'post',
        data: {
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password,
        },
        url: `${baseUrl}/api/users/register`,
    };
    try {
        const response = await axios(configurationObject);
        return response;
    } catch (error: any) {
        return error.response;
    }
};

export const searchRoomRequest = async (accessToken: String, guestNumber: Int32, date: String, startTime: String, endTime: String) => {
    
    // const userData = useSelector((state: any) => state.roomSlice.avilableRooms);
    const configurationObject = {
        method: 'post',
        data: {
            guestNumber: guestNumber,
            date: date,
            startTime: startTime,
            endTime: endTime,
        },
        headers: {
            Authorization: `Bearer ${accessToken}`
        },
        url: `${baseUrl}/api/rooms/search`,
    };
    try {
        const response = await axios(configurationObject);
        return response;
    } catch (error: any) {
        return error.response;
    }
};

export const makeBookingRoomRequest = async (accessToken: String, roomId: number, guestNumber: Int32, date: String, startTime: String, endTime: String) => {
    
    // const userData = useSelector((state: any) => state.roomSlice.avilableRooms);
    const configurationObject = {
        method: 'post',
        data: {
            title: "Booking " + roomId + " Room" + " for " + guestNumber + " people" + " at " + date + " from " + startTime + " to " + endTime,
            guestNumber: guestNumber,
            date: date,
            startTime: date + "T" + startTime,
            endTime:  date + "T" + endTime,
        },
        headers: {
            Authorization: `Bearer ${accessToken}`
        },
        url: `${baseUrl}/api/rooms/`+ roomId +`/book`,
    };
    try {
        const response = await axios(configurationObject);
        return response;
    } catch (error: any) {
        return error.response;
    }
};

export const getMyBookingRequest = async (accessToken: String) => {
    const configurationObject = {
        method: 'get',
        headers: {
            Authorization: `Bearer ${accessToken}`
        },
        url: `${baseUrl}/api/bookings/`,
    };
    try {
        const response = await axios(configurationObject);
        return response;
    } catch (error: any) {
        return error.response;
    }
}

export const cancelMyBookingRequest = async (accessToken: String, bookingId: number) => {
    const configurationObject = {
        method: 'delete',
        headers: {
            Authorization: `Bearer ${accessToken}`
        },
        url: `${baseUrl}/api/bookings/${bookingId}`,
    };
    try {
        const response = await axios(configurationObject);
        return response;
    } catch (error: any) {
        return error.response;
    }
}
