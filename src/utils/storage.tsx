import AsyncStorage from '@react-native-async-storage/async-storage';


const storeStringData = async (key: string, value: string) => {
    try {
        await AsyncStorage.setItem(key, value);
    } catch (e) {
      // saving error
    }
};

const storeObjectData = async (key: string, value: Object) => {
    try {
        const jsonValue = JSON.stringify(value);
        await AsyncStorage.setItem(key, jsonValue);
    } catch (e) {
      // saving error
    }
};

const getStringData = async (key: string) => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        return value;
      }
    } catch (e) {
        // error reading value
        return null;
    }
};

const getObjectData = async (key: string) => {
    try {
        const jsonValue = await AsyncStorage.getItem(key);
        return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
        // error reading value
        return null;
    }
};

const removeData = async (key: string) => {
    try {
        await AsyncStorage.removeItem(key);
    } catch (e) {
        // remove error
    }
};

export { 
    storeStringData, 
    storeObjectData, 
    getStringData, 
    getObjectData, 
    removeData 
};
