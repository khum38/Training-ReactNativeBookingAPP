import { View, StyleSheet } from 'react-native';

interface DrawerAppProps {
    children: any;
    height?: number;
}

const DrawerApp = (props: DrawerAppProps) => {
    const styles = StyleSheet.create({
        drawerBody: {
            position: 'absolute',
            bottom: 0,
            left: 0,
            width: '100%',
            backgroundColor: '#FFFFFF',
            paddingHorizontal: 22,
            paddingVertical: 40,
            height: props.height ? props.height : 450,
            borderTopLeftRadius: 30,
            borderTopRightRadius: 30,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
        },
    });

    return (
        <View style={styles.drawerBody}>
            {props.children}
        </View>
    );
}

export default DrawerApp;