import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    imageBackground: {
        flex: 1,
        resizeMode: 'cover',
    },
    overlayImage: {
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        fontFamily: "Open Sans"
    },
});

export default styles;
