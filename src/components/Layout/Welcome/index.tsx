import { View, Text, StyleSheet, ImageBackground, Image } from 'react-native';

import styles from './styles';

interface WelcomeLayoutProps {
    children: any;
    imageBackground?: any;
    useOverlay?: boolean;
}

const WelcomeLayout = (props: WelcomeLayoutProps) => {
    return (
        <ImageBackground
        // require('../../../assets/images/background/pawel-chu-ULh0i2txBCY-unsplash-2.png')
            source={props.imageBackground} // Replace this with the path to your image
            style={styles.imageBackground}
        >
            { props.useOverlay && <Image
                source={require('../../../assets/images/background/Rectangle-96.png')} // Replace this with the path to your overlay image
                style={styles.overlayImage}
            /> }
            <View style={styles.container}>
                {props.children}
            </View>
        </ImageBackground>
    );
}



export default WelcomeLayout;