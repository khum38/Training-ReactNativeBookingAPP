import { Text } from 'native-base';
import { View, TouchableOpacity, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';

// import { removeData } from '../../utils/storage';
import { resetData } from '../../stores/slice/authSlice';
import { RootStackNavigationProps } from '../../common';

import styles from './styles';

interface HeaderProps {
    title: string;
    children?: any;
    imageBackground?: any;
    style?: any;
}

const Header = (props: HeaderProps) => {
    const navigation = useNavigation();

    const logoutPress = () => {
        // removeData('access_token');
        // removeData('refresh_token');
        // removeData('user_email');
        

        navigation.navigate(RootStackNavigationProps.WelcomeNavigator.name);
    }

    return (
        <View style={props.style}>
            <View style={styles.headerContainer}>
                <Image
                    // source={require('../../assets/images/background/pawel-chu-ULh0i2txBCY-unsplash-1.png')} 
                    source={props.imageBackground}
                    style={styles.headerImage}
                ></Image>
                <Image
                    source={require('../../assets/images/background/Rectangle-96.png')} // Replace this with the path to your overlay image
                    style={styles.overlayImage}
                />
                <TouchableOpacity 
                    onPress={logoutPress} 
                    activeOpacity={1}
                    style={styles.logoutButton}
                    testID='LogoutButton'
                >
                    <Text fontFamily="body" fontWeight={400} style={styles.logoutButtonText}>Logout</Text>
                </TouchableOpacity>

                <View>
                    <Text fontFamily="body" fontWeight={600} style={styles.titleText}>{props.title}</Text>
                </View>
            </View>
            
            <View >
                {props.children}
            </View>
        </View>
    );
}



export default Header;