import { StyleSheet } from "react-native";


const styles = StyleSheet.create({
    headerImage: {
        position: 'absolute',
        resizeMode: 'cover',
        // justifyContent: 'center',
        // overflow: 'hidden',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        width: '100%',
        height: 120,
    },
    overlayImage: {
        flex: 1,
      position: 'absolute',
      width: '100%',
      height: 120,
      borderBottomLeftRadius: 20,
      borderBottomRightRadius: 20,
    },
    titleText: {
        fontSize: 28,
        lineHeight: 42,
        color: '#FFFFFF',
        marginTop: 62,
        marginLeft: 22,
    },
    logoutButton: {
        position: 'absolute',
        top: 40,
        right: 20,
        height: 24,
        width: 73,
        zIndex: 1,
    },
    logoutButtonText: {
        lineHeight: 24,
        fontSize: 16,
        color: '#FFFFFF',
    },
    headerContainer: {
        top: 0,
        left: 0,
        height: 120,
        // flex: 1,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        // flexDirection:'row'
        // justifyContent: 'center',
        // fontFamily: "Open Sans"
    },
});

export default styles;
