import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    modalContent: {
        // backgroundColor: 'white',
        // padding: 20,
        borderRadius: 20,
        margin: 0,
        width: "100%",
    },
    modalBody: {
        flex: 1, 
        // borderRadius: 20,
        height: "100%",
        flexDirection: 'column', // Use flexDirection to arrange elements horizontally
        //   alignItems: 'center', // Vertically center the content
          // justifyContent: 'flex-end',
        justifyContent: 'space-between',
    },
    modalFooter: {
      width: "100%",
      backgroundColor: 'white',
      borderColor: "white", 
      justifyContent: 'space-between',
      paddingBottom: 40
    },
});

export default styles;
