import { Modal } from 'native-base';

import styles from './styles';

interface ModalBottomProps {
    children: any;
    footer: Element;
    isOpen: boolean;
    overlayVisible?: boolean;
}

const ModalBottom = (props: ModalBottomProps) => {

    return (
        <Modal 
            isOpen={props.isOpen} 
            // onClose={() => setModalVisible(false)} 
            avoidKeyboard 
            justifyContent="flex-end" 
            // size="fullWidth"
            overlayVisible={props.overlayVisible}
            bottom={0}
            style={{ 
                marginBottom: 0,
                marginTop: "auto",
                backgroundColor: 'white',
                borderRadius: 20,
            }}
            height="auto"
        >
            <Modal.Content style={styles.modalContent}>
                <Modal.Body style={styles.modalBody}>
                    {props.children}
                </Modal.Body>
            </Modal.Content>
            { props.footer && 
                <Modal.Footer style={styles.modalFooter}>{props.footer}
                </Modal.Footer> 
            }
        </Modal>
    );
}

export default ModalBottom;