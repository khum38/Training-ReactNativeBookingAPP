import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    labelText: {
        fontSize: 16,
        paddingVertical: 10,
    },
    bookingCardNameText: {
        // flex: 1,
        color: "#191919",
        lineHeight: 30,
        // fontSize: 20,
    },
    bookingCardBody: {
        flexDirection: "column",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#191919",
        backgroundColor: "#FFFFFF",
        padding: 22,
        marginVertical: 10,
    },
});

export default styles;
