import { Text } from 'native-base';
import { View } from 'react-native';

import ButtonApp from '../../components/ButtonApp';
import styles from './styles';

interface BookingCardProps {
    data: any;
    testID: string;
    onCancelButtonClick: () => void;
}

const BookingCard = (props: BookingCardProps) => {

    const startDateTimeShow = props.data["startDateTime"].split("T")[1].split(":")
    const endDateTimeShow = props.data["endDateTime"].split("T")[1].split(":")

    return (
        <View testID={props.testID} style={styles.bookingCardBody}>
            <View >
                <Text fontFamily="body" fontWeight={700} color={"secondary.100"} style={styles.labelText} >Booking No. {props.data["id"]}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                    <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.bookingCardNameText} >
                        {props.data["room"]["name"]}
                    </Text>
                    <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.bookingCardNameText} >
                        {props.data["date"].split("T")[0]}
                    </Text>
                </View>
                <View style={{ flex: 1 }}>
                    <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.bookingCardNameText} >
                        {/* {props.data["guestNumber"]} Guests */}
                        {props.data["room"]["capacity"]} Guests max
                    </Text>
                    <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.bookingCardNameText} >
                        {startDateTimeShow[0] + ":" + startDateTimeShow[1]} - {endDateTimeShow[0] + ":" + endDateTimeShow[1]}
                    </Text>
                </View>
            </View>
            <View style={{ marginTop: 10 }}>
                <ButtonApp 
                    testID={props.testID + '_CancelBooking_button'}
                    text="Cancel"
                    onPress={() => {
                        props.onCancelButtonClick()
                        // setModalVisible(false)
                        // goToFindRoomScreen()
                    }}
                    type={"red"}
                    styleBody={{ height: 40 }}
                />
            </View>
        </View>
    );
}

export default BookingCard;