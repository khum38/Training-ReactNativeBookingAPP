import { Text } from 'native-base';
import { View, TouchableOpacity } from 'react-native';

import styles from './styles';

interface RoomSelectCardProps {
    data: any;
    onPress: () => void;
    testID: string;
}

const RoomSelectCard = (props: RoomSelectCardProps) => {

    return (
        <TouchableOpacity 
            testID={props.testID}
            style={styles.roomCardBody}
            onPress={props.onPress}
        >
            <Text fontFamily="body" fontWeight={600} style={styles.roomCardNameText} >{props.data["name"]}</Text>
            <Text fontFamily="body" fontWeight={400} style={styles.roomCardInfoText} >{props.data["capacity"] + ` Guests max`}</Text>
        </TouchableOpacity>
    );
}

export default RoomSelectCard;