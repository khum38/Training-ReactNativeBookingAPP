import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    roomCardNameText: {
        flex: 1,
        color: "#191919",
        lineHeight: 30,
        fontSize: 20,
    },
    roomCardInfoText: {
        flex: 1,
        color: "#191919",
        lineHeight: 30,
        fontSize: 16,
    },
    roomCardBody: {
        flexDirection: "row",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#191919",
        backgroundColor: "#FFFFFF",
        padding: 22,
        marginVertical: 10,
    },
});

export default styles;
