import { Text } from 'native-base';
import { StyleSheet, TouchableOpacity } from 'react-native';

interface ButtonAppProps {
    text: String;
    onPress?: () => void;
    type?: "green" | "white" | "inactive" | "inactive-white" | "red" | "red-white";
    styleBody?: any;
    styleText?: any;
    testID?: string;
}

const ButtonApp = (props: ButtonAppProps) => {

    let buttonBackgroundColor, buttonTextColor, buttonBorderColor;
    if (props.type == "green") {
        buttonBackgroundColor = "#5CC99B";
        buttonTextColor = "#FFFFFF";
        buttonBorderColor = "#5CC99B";
    } else if (props.type == "white") {
        buttonBackgroundColor = "#FFFFFF";
        buttonTextColor = "#5CC99B";
        buttonBorderColor = "#5CC99B";
    } else if (props.type == "inactive") {
        buttonBackgroundColor = "#D3D3CE";
        buttonTextColor = "#FFFFFF";
        buttonBorderColor = "#D3D3CE";
    } else if (props.type == "inactive-white") {
        buttonBackgroundColor = "#FFFFFF";
        buttonTextColor = "#AAA8A3";
        buttonBorderColor = "#AAA8A3";
    } else if (props.type == "red") {
        buttonBackgroundColor = "#EA5C65";
        buttonTextColor = "#FFFFFF";
        buttonBorderColor = "#EA5C65";
    } else if (props.type == "red-white") {
        buttonBackgroundColor = "#FFFFFF";
        buttonTextColor = "#EA5C65";
        buttonBorderColor = "#EA5C65";
    }

    let styles = StyleSheet.create({
        buttonContainer: {
            borderWidth: 1,
            borderRadius: 10,
            alignItems: 'center',
            justifyContent: 'center',
            fontSize: 16,
            height: 75,
            borderColor: buttonBorderColor,
            backgroundColor: buttonBackgroundColor,
        },
        buttonText: {
            fontSize: 16,
            fontWeight: 'bold',
            color: buttonTextColor,
        },
    });

    return (
        <TouchableOpacity
            testID={props.testID}
            style={Object.assign({}, styles.buttonContainer, props.styleBody)}
            activeOpacity={0.8}
            onPress={props.onPress}
            disabled={(props.type == "inactive" || props.type == "inactive-white")  ? true : false}
        >
            <Text 
                fontFamily="body" 
                style={Object.assign({}, styles.buttonText, props.styleText)}
            >{props.text}</Text>
        </TouchableOpacity>
    )
};

export default ButtonApp;