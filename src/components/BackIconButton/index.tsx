import { TouchableOpacity, Image, StyleSheet } from 'react-native';

interface BackIconButtonProps {
    onPress: any;
    style?: any;
}

const BackIconButton = (props: BackIconButtonProps) => {

    const styles = StyleSheet.create({
        backButton: {
            top: 45,
            left: 22,
            height: 15,
            width: 15,
            position: 'absolute',
        },
        backIconButton: {
            height: 15,
            width: 15,
        },
    });
    
    return (
        <TouchableOpacity 
            onPress={props.onPress} 
            activeOpacity={1}
            style={Object.assign({}, styles.backButton, props.style)}
        >
            <Image
                source={require('../../assets/images/icons/Arrow-left.png')} // Replace this with the path to your logo image
                style={styles.backIconButton}
            />
        </TouchableOpacity>
    );
}


export default BackIconButton;
