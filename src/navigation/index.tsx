import React from 'react'
import { NavigationContainer } from '@react-navigation/native'

import HomeScreen from '../screens/HomeScreen';
import LoginScreen from '../screens/LoginScreen';
import CreateAccountScreen from '../screens/CreateAccountScreen';
import FindRoomScreen from '../screens/FindRoomScreen';
import MyBookingScreen from '../screens/MyBookingScreen';
import SelectRoomScreen from '../screens/SelectRoomScreen';
import BookingSummaryScreen from '../screens/BookingSummaryScreen';
import MakeMyBookingScreen from '../screens/MakeMyBookingScreen';

import { Stack, RootStackNavigationProps } from '../common'


export const RootNavigator = () => {

    return (
        <NavigationContainer>
            <Stack.Navigator
                initialRouteName={RootStackNavigationProps.WelcomeNavigator.name}
            >
                <Stack.Screen {...RootStackNavigationProps.WelcomeNavigator} component={HomeScreen} />
                <Stack.Screen {...RootStackNavigationProps.LoginNavigator} component={LoginScreen} />
                <Stack.Screen {...RootStackNavigationProps.CreateAccountNavigator} component={CreateAccountScreen} />
                <Stack.Screen {...RootStackNavigationProps.FindRoomNavigator} component={FindRoomScreen} />
                <Stack.Screen {...RootStackNavigationProps.SelectRoomNavigator} component={SelectRoomScreen} />
                <Stack.Screen {...RootStackNavigationProps.BookingSummaryNavigator} component={BookingSummaryScreen} />
                <Stack.Screen {...RootStackNavigationProps.MakeMyBookingNavigator} component={MakeMyBookingScreen} />

                <Stack.Screen {...RootStackNavigationProps.MyBookingNavigator} component={MyBookingScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}