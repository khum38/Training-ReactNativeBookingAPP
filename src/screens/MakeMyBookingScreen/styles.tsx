import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    titleTextView: {
        // top: 62,
        // left: 22,
        position: 'absolute',
        width: "100%",
    },
    titleText: {
        width: "100%",
        textAlign: "center",
        fontSize: 28,
        lineHeight: 42,
        // fontWeight: 'bold',
        color: '#FFFFFF',
    },
    infoText: {
        width: "100%",
        textAlign: "center",
        fontSize: 20,
        lineHeight: 30,
        // fontWeight: 'bold',
        color: '#FFFFFF',
    },
});

export default styles;
