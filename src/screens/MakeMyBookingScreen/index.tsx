import React from 'react';
import { Text } from 'native-base';
import { View } from 'react-native';
import { useDispatch, useSelector } from "react-redux";

import { useNavigation } from '@react-navigation/native';
import { RootStackNavigationProps } from '../../common';

import WelcomeLayout from '../../components/Layout/Welcome';
import ButtonApp from '../../components/ButtonApp';
import ModalBottom from '../../components/ModalBottom';

import styles from './styles';

const MakeMyBookingScreen: React.FC = () => {
    const navigation = useNavigation();
    const [modalVisible, setModalVisible] = React.useState<boolean>(true);

    const goToMyBookingScreen = () => {
        navigation.navigate(RootStackNavigationProps.MyBookingNavigator.name);
    }

    const lastBooking = useSelector((state: any) => state.room["lastBooking"]);


    return (
        <WelcomeLayout 
            imageBackground={require('../../assets/images/background/damir-kopezhanov-VM1Voswbs0A-unsplash-1.png')}
            useOverlay={false}
        >
            <View style={styles.titleTextView}>
                <View>
                    <Text testID='MakeMyBookingScreen' fontFamily="body" fontWeight={600} style={styles.titleText}>Booking</Text>
                    <Text fontFamily="body" fontWeight={600} style={styles.titleText}>Successful</Text>
                </View>
                <View style={{ marginTop: 20 }}>
                    <Text fontFamily="body" fontWeight={400} style={styles.infoText}>Your Booking No. is</Text>
                    <Text fontFamily="body" fontWeight={400} style={styles.infoText}>{lastBooking["id"]}</Text>
                </View>
            </View>

            <ModalBottom
                isOpen={modalVisible}
                footer={false}
                overlayVisible={false}
            >
                <View style={{ marginVertical: 10 }}>
                    <ButtonApp 
                        text="My Booking History"
                        testID='GoToMyBookingHistory_button'
                        onPress={() => {
                            setModalVisible(false)
                            goToMyBookingScreen()
                        }}
                        type={"green"}
                    />
                </View>
            </ModalBottom>
        </WelcomeLayout>
    );
};

export default MakeMyBookingScreen;
