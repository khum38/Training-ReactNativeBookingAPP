import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    bodyScreen: {
        flex: 1,
    },
    container: {
        // display: 'flex',
        padding: 23,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        // height: '100%',
    },
    fromViewBody: {
        marginVertical: 30,
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    fromControl: {
        marginBottom: 20,
    },
    input: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#706F68',
        height: 60,
    },
    labelInput: {
        fontSize: 16,
    },
    mybookingButton: {
        // flex: 1,
        flexDirection: 'row', // Use flexDirection to arrange elements horizontally
        alignItems: 'center', // Vertically center the content
        justifyContent: 'flex-end',
    },
    mybookingTouchableOpacity: {
        flexDirection: 'row'
    },
    mybookingText: {
        marginLeft: 5,
        fontSize: 16,
        lineHeight: 24,
        textAlign: 'right',
        height: 24,
        verticalAlign: 'middle',
    },
    calendarIconButton: {
        height: 20,
        width: 18,
        resizeMode: 'contain',
    },
    mybookingButtonContent: {
        flexDirection: 'row', // Arrange image and text horizontally
        alignItems: 'center',
    },
    findRoomBody: {
        flex: 1, 
        justifyContent: 'flex-end',
        marginBottom: 20,
    },
    inputButtonDone: {
        flex: 1,
        height: 75,
    },
});

export default styles;
