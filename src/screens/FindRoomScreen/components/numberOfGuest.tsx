import { StyleSheet, View } from 'react-native';
import { Text } from 'native-base';

import ButtonApp from '../../../components/ButtonApp';


interface NumberOfGuestBodyProps {
    updateGuestNumberInput: (increase: boolean) => void;
    guestNumberInput: number;
}

const NumberOfGuestBody = (props: NumberOfGuestBodyProps) => {

    return (
        <View style={styles.numberGuestInputBody} testID='numberOfGuest_body'>
            <Text 
                fontFamily="body" fontWeight={400} color={"primary.100"} style={styles.numberGuestInputLabel}
            >Number of guest</Text>
            <View style={styles.numberGuestInputButtonGroup}>
                <ButtonApp 
                    text="-"
                    testID='numberOfGuest_button_minus_input'
                    onPress={() => {props.updateGuestNumberInput(false)}}
                    type={(props.guestNumberInput <= 0) ? "inactive-white" : "green"}
                    styleBody={styles.numberGuestInputButtonNumber}
                />
                <Text 
                    fontFamily="body" fontWeight={400} color={"secondary.100"} style={{lineHeight: 35}}>
                    {props.guestNumberInput}
                </Text>
                <ButtonApp 
                    text="+"
                    testID='numberOfGuest_button_plus_input'
                    onPress={() => {props.updateGuestNumberInput(true)}}
                    type={"green"}
                    styleBody={styles.numberGuestInputButtonNumber}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    numberGuestInputLabel: {
        flex: 2,
        lineHeight: 35,
    },
    numberGuestInputButtonGroup: {
        flex: 1, 
        flexDirection: "row",
        justifyContent: 'space-between',
    },
    numberGuestInputButtonNumber: {
        height: 35, 
        width: 35,
        borderRadius: 5
    },
    numberGuestInputBody: {
        flex: 1,
        flexDirection: 'row',
    },
});

export default NumberOfGuestBody;