import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import { Text, FormControl, Input, Modal } from 'native-base';
import { Calendar, LocaleConfig } from 'react-native-calendars';
// import RNDateTimePicker from '@react-native-community/datetimepicker';
import DatePicker from 'react-native-date-picker'

import { useNavigation } from '@react-navigation/native';

import { useDispatch, useSelector } from "react-redux";
import { setAvilableRooms } from '../../stores/slice/roomSlice';

import { searchRoomRequest } from '../../apiRequests';

import { RootStackNavigationProps } from '../../common';

import Header from '../../components/Header';
import ButtonApp from '../../components/ButtonApp';
import ModalBottom from '../../components/ModalBottom';

import NumberOfGuestBody from './components/numberOfGuest';

import styles from './styles';


const FindRoomScreen: React.FC = () => {
    const navigation = useNavigation();
    const [guestNumberInput, setGuestNumberInput] = React.useState<number>(1);
    const [dateInput, setDateInput] = React.useState<string>(new Date().toISOString().split('T')[0]);
    const [startTimeInput, setStartTimeInput] = React.useState<Date>(roundToNearestHalfHour(new Date()));

    const startTimeStart = new Date();
    startTimeStart.setMinutes(startTimeStart.getMinutes() + 30);
    
    const [endTimeInput, setEndTimeInput] = React.useState<Date>(roundToNearestHalfHour(startTimeStart));
    const [modalVisible, setModalVisible] = React.useState<boolean>(false);
    const [modalShowBody, setModalSowBody] = React.useState<string>("");

    const dispatch = useDispatch();


    const accessToken = useSelector((state: any) => state.auth["accessToken"]);


    const headerImagePath = require('../../assets/images/background/pawel-chu-ULh0i2txBCY-unsplash-1.png');


    const goToMyBookingScreen = () => {
        navigation.navigate(RootStackNavigationProps.MyBookingNavigator.name);
    }

    const goToSelectRoomScreen = () => {
        navigation.navigate(RootStackNavigationProps.SelectRoomNavigator.name);
    }

    function roundToNearestHalfHour(date: Date) {
        const roundedDate = new Date(date);
        const minutes = roundedDate.getMinutes();
      
        // Round up to the next half-hour if minutes are greater than 15 and less than 45
        if (minutes < 30) {
          roundedDate.setMinutes(30);
        } else if (minutes >= 30) { // Round up to the next hour if minutes are greater than or equal to 45
          roundedDate.setMinutes(0);
          roundedDate.setHours(roundedDate.getHours() + 1);
        } else { // Otherwise, round down to the previous half-hour
          roundedDate.setMinutes(0);
        }
      
        return roundedDate;
    }

    const searchAvliableRoom = async () => {
        const startTimeString = startTimeInput.getHours().toString().padStart(2, '0') + ":" + startTimeInput.getMinutes().toString().padStart(2, '0');
        const endTimeString = endTimeInput.getHours().toString().padStart(2, '0') + ":" + endTimeInput.getMinutes().toString().padStart(2, '0');
        const startTimeRequest = dateInput + "T" + startTimeString;
        const endTimeRequest =  dateInput + "T" + endTimeString;
        const response = await searchRoomRequest(accessToken, guestNumberInput, dateInput, startTimeRequest, endTimeRequest);

        if(response.status == 200){
            const avilableRooms = {
                "guestNumberInput": guestNumberInput,
                "dateInput": dateInput,
                "startTimeInput": startTimeString,
                "endTimeInput": endTimeString,
                "rooms": response.data
            }
            dispatch(setAvilableRooms(avilableRooms));
            goToSelectRoomScreen()
        } else {
        }
    }

    const updateGuestNumberInput = (increase: boolean) => {
        if (increase) {
            setGuestNumberInput(guestNumberInput + 1);
        } else if (guestNumberInput > 0) {
            setGuestNumberInput(guestNumberInput - 1);
        }
    }

    const showInputModal = (inputField: string) => {
        setModalSowBody(inputField);

        if (inputField){
            setModalVisible(true);
        }else{
            setModalVisible(false);
        }
    }

    return (
        <View style={styles.bodyScreen}>
            <Header 
                title={"Make My Booking"} 
                imageBackground={headerImagePath}
            />
            <View style={styles.container} testID="FindRoomScreen">
                <View style={styles.mybookingButton}>
                    <TouchableOpacity onPress={goToMyBookingScreen} style={styles.mybookingTouchableOpacity} >
                        <View style={styles.mybookingButtonContent}>
                            <Image
                                source={require('../../assets/images/icons/Calendar-Check.png')} // Replace this with the path to your logo image
                                style={styles.calendarIconButton}
                            />
                            <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.mybookingText}>
                                My Bookings
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.fromViewBody}>
                    <FormControl style={styles.fromControl}>
                        <FormControl.Label >
                            <Text fontFamily="body" fontWeight={400} color={"primary.100"} style={styles.labelInput}>Number of Guest</Text>
                        </FormControl.Label>
                        <Input 
                            autoCapitalize="none"
                            size="2xl" placeholder="Select number of guest" style={styles.input} 
                            borderRadius={10}
                            isReadOnly={true}
                            testID='findingroom_guestnumber_input'
                            value={guestNumberInput.toString()}
                            onTouchStart={() => {showInputModal("guestNumberInput")}}
                        />
                    </FormControl>

                    <FormControl style={styles.fromControl}>
                        <FormControl.Label >
                            <Text fontFamily="body" fontWeight={400} color={"primary.100"} style={styles.labelInput}>Date</Text>
                        </FormControl.Label>
                        <Input 
                            autoCapitalize="none"
                            size="2xl" placeholder="Select date" style={styles.input} 
                            borderRadius={10}
                            isReadOnly={true}
                            testID='findingroom_date_input'
                            value={dateInput.toString()}
                            onTouchStart={() => {showInputModal("dateInput")}}
                        />
                    </FormControl>
                    <FormControl style={styles.fromControl}>
                        <FormControl.Label >
                            <Text fontFamily="body" fontWeight={400} color={"primary.100"} style={styles.labelInput}>Start Time</Text>
                        </FormControl.Label>
                        <Input 
                            autoCapitalize="none"
                            size="2xl" placeholder="Select start time" style={styles.input} 
                            borderRadius={10}
                            isReadOnly={true}
                            testID='findingroom_starttime_input'
                            value={startTimeInput.getHours().toString().padStart(2, '0') + ":" + startTimeInput.getMinutes().toString().padStart(2, '0')}
                            onTouchStart={() => {showInputModal("startTimeInput")}}
                        />
                    </FormControl>

                    <FormControl style={styles.fromControl}>
                        <FormControl.Label >
                            <Text fontFamily="body" fontWeight={400} color={"primary.100"} style={styles.labelInput}>End Time</Text>
                        </FormControl.Label>
                        <Input 
                            autoCapitalize="none"
                            size="2xl" placeholder="Select end time" style={styles.input} 
                            borderRadius={10}
                            isReadOnly={true}
                            testID='findingroom_endtime_input'
                            value={endTimeInput.getHours().toString().padStart(2, '0') + ":" + endTimeInput.getMinutes().toString().padStart(2, '0')}
                            onTouchStart={() => {showInputModal("endTimeInput")}} 
                        />
                    </FormControl>
                </View>
                <View style={styles.findRoomBody}>
                    <ButtonApp 
                        text="Find Room"
                        testID='findingroom_Button'
                        onPress={searchAvliableRoom}
                        type={(guestNumberInput && dateInput && startTimeInput && endTimeInput && (endTimeInput > startTimeInput)) ? "green" : "inactive"}
                        styleBody={{}}
                    />
                </View>
            </View>

            <ModalBottom
                isOpen={modalVisible}
                overlayVisible={false}
                footer={<ButtonApp 
                    text="Done"
                    testID='findingroom_done_button'
                    onPress={() => {
                        setModalVisible(false);
                    }}
                    type={"green"}
                    styleBody={styles.inputButtonDone}
                />}
            >
                {modalShowBody == "guestNumberInput" && <NumberOfGuestBody
                    updateGuestNumberInput={updateGuestNumberInput}
                    guestNumberInput={guestNumberInput}
                />}
                {modalShowBody == "dateInput" && <Calendar
                    onDayPress={day => {
                        setDateInput(day.dateString);
                    }}
                    markedDates={{
                        [dateInput]: {selected: true, disableTouchEvent: true, selectedColor: 'green'}
                    }}
                />}
                {modalShowBody == "startTimeInput" && <View>
                    <DatePicker 
                        testID='findingroom_starttime_datepicker_input'
                        date={startTimeInput} mode="time" 
                        onDateChange={setStartTimeInput} 
                    />
                </View>}
                {modalShowBody == "endTimeInput" && <View>
                    <DatePicker 
                        testID='findingroom_starttime_datepicker_input'
                        date={endTimeInput} 
                        mode="time" 
                        onDateChange={setEndTimeInput} 
                    />
                </View>}
            </ModalBottom>
        </View>
    )
}

export default FindRoomScreen;
