import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    labelInput: {
        fontSize: 16,
    },
    input: {
        borderRadius: 10,
        height: 60,
    },
    titleTextView: {
        top: 102,
        left: 22,
        position: 'absolute',
    },
    titleText: {
        fontSize: 28,
        lineHeight: 42,
        fontWeight: 'bold',
        color: '#FFFFFF',
    },
    text: {
        fontSize: 16,
    },
    errorBox: {
        backgroundColor: 'rgba(234, 92, 101,0.3)',
        borderRadius: 10,
        paddingHorizontal: 26,
        paddingVertical: 10,
    }
    
});

export default styles;
