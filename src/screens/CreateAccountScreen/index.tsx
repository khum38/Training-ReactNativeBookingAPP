import React from 'react';
import { useState } from 'react';
import { Input, Text, FormControl } from 'native-base';
import { View } from 'react-native';
// import { Input } from 'native-base';
import { useNavigation } from '@react-navigation/native';

import { useDispatch, useSelector } from "react-redux";
import { setAccessToken, setUserEmail } from '../../stores/slice/authSlice';

import { registerAccountRequest } from '../../apiRequests';

import DrawerApp from '../../components/Drawer';
import WelcomeLayout from '../../components/Layout/Welcome';
import BackIconButton from '../../components/BackIconButton';
import ButtonApp from '../../components/ButtonApp';

import { RootStackNavigationProps } from '../../common';
import styles from './styles';

const CreateAccountScreen: React.FC = () => {
    const [showError, setShowError] = useState<boolean>(false); // [error, setError
    const [emailInput, setEmailInput] = useState<string>('');
    const [passwordInput, setPasswordInput] = useState<string>('');
    const [firstnameInput, setFirstnameInput] = useState<string>('');
    const [lastnameInput, setLastnameInput] = useState<string>('');

    const navigation = useNavigation();
    const dispatch = useDispatch();

    const goToWelcomeScreen = () => {
        navigation.navigate(RootStackNavigationProps.WelcomeNavigator.name);
    };

    const goToFindRoomScreen = () => {
        navigation.navigate(RootStackNavigationProps.FindRoomNavigator.name);
    };

    const signUp = async () => {  
        const response = await registerAccountRequest(firstnameInput, lastnameInput, emailInput, passwordInput);
        if(response.status == 200){
            dispatch(setAccessToken(response.data.access_token));
            dispatch(setUserEmail(emailInput));
            goToFindRoomScreen();
        } else {
            setShowError(true);
        }
    };

  return (
    <WelcomeLayout 
        imageBackground={require('../../assets/images/background/pawel-chu-ULh0i2txBCY-unsplash-2.png')}
        useOverlay={true}
    >
        <BackIconButton onPress={goToWelcomeScreen} />
        <View style={styles.titleTextView} testID="CreateAccountScreen">
            <Text style={styles.titleText}>Create</Text>
            <Text style={styles.titleText}>New Account</Text>
        </View>
        <DrawerApp height={562}>
            {showError && <View style={styles.errorBox}>
                <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.text}>
                    Please try again.
                </Text>
            </View>}
            <View>
                <FormControl>
                    <FormControl.Label >
                        <Text fontFamily="body" fontWeight={400} color={"primary.100"} style={styles.labelInput}>First Name</Text>
                    </FormControl.Label>
                    <Input 
                        autoCapitalize="none"
                        borderRadius={10}
                        testID="signup_textfield_firstname"
                        size="2xl" placeholder="First Name" style={styles.input} 
                        onChangeText={(value) => {setFirstnameInput(value)}} 
                    />
                </FormControl>
                <FormControl>
                    <FormControl.Label >
                        <Text fontFamily="body" fontWeight={400} color={"primary.100"} style={styles.labelInput}>Last Name</Text>
                    </FormControl.Label>
                    <Input 
                        autoCapitalize="none"
                        borderRadius={10}
                        testID="signup_textfield_lastname"
                        size="2xl" placeholder="Last Name" style={styles.input} 
                        onChangeText={(value) => {setLastnameInput(value)}} 
                    />
                </FormControl>
                <FormControl>
                    <FormControl.Label >
                        <Text fontFamily="body" fontWeight={400} color={"primary.100"} style={styles.labelInput}>Email</Text>
                    </FormControl.Label>
                    <Input 
                        autoCapitalize="none"
                        borderRadius={10}
                        testID="signup_textfield_email"
                        size="2xl" placeholder="Email" style={styles.input} 
                        onChangeText={(value) => {setEmailInput(value)}} 
                    />
                </FormControl>

                <FormControl style={{marginTop: 10}}>
                    <FormControl.Label >
                        <Text fontFamily="body" fontWeight={400} color={"primary.100"} style={styles.labelInput}>Password</Text>
                    </FormControl.Label>
                    <Input 
                        size="2xl" 
                        borderRadius={10}
                        placeholder="Password" 
                        type='password' 
                        testID="signup_textfield_password"
                        style={styles.input} 
                        onChangeText={(value) => {setPasswordInput(value)}} 
                    />
                </FormControl>
            </View>
            <ButtonApp 
                text="Create Account"
                onPress={signUp}
                type={(emailInput && passwordInput && firstnameInput && lastnameInput) ? "green" : "inactive"}
            />
        </DrawerApp>
    </WelcomeLayout>
  );
};

export default CreateAccountScreen;
