import React from 'react';
import { Text } from 'native-base';
import { View, StyleSheet, Image, Platform } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import ButtonApp from '../../components/ButtonApp';
import DrawerApp from '../../components/Drawer';
import WelcomeLayout from '../../components/Layout/Welcome';

import { RootStackNavigationProps } from '../../common';

import styles from './styles';

const HomeScreen: React.FC = () => {
  const navigation = useNavigation();

  const goToLoginScreen = () => {
    navigation.navigate(RootStackNavigationProps.LoginNavigator.name);
  };

  const goToCreateAccountScreen = () => {
    navigation.navigate(RootStackNavigationProps.CreateAccountNavigator.name);
  };

  const textTitle = "Let’s make a meeting room booking easier.";

  return (
    <WelcomeLayout 
      imageBackground={require('../../assets/images/background/pawel-chu-ULh0i2txBCY-unsplash-2.png')}
      useOverlay={true}
    >
        <Image
            source={require('../../assets/images/logo/PALO-LOGO-Colour-white.png')} // Replace this with the path to your logo image
            style={styles.logo}
        />
        <View style={styles.titleTextView}>
            <Text testID='LandingScreen' fontFamily="body" fontWeight={600} style={styles.titleText}>Meeting</Text>
            <Text fontFamily="body" fontWeight={600} style={styles.titleText}>Room Booking</Text>
        </View>
        <DrawerApp>
            <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.text}>{textTitle}</Text>
            <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.text}>Meeting Room Booking will help you to ensure you will have a room for your meeting. Manage reservation, cancellation. ongogin or finised booking.</Text>
            <View style={{marginTop: 20}}>
                <ButtonApp 
                    testID="GoToLoginButton" 
                    text="Login"
                    onPress={goToLoginScreen}
                    type="green"
                    styleBody={{marginTop: 10}}
                />

                <ButtonApp 
                    testID="GoToSignupButton" 
                    text="Sign Up"
                    onPress={goToCreateAccountScreen}
                    type="white"
                    styleBody={{marginTop: 10}}
                />
            </View>
        </DrawerApp>
    </WelcomeLayout>
  );
};

export default HomeScreen;
