import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    logo: {
        top: 50,
        left: 22,
        height: 60,
        width: 125,
        position: 'absolute',
    },
    titleTextView: {
        top: 130,
        left: 22,
        position: 'absolute',
    },
    titleText: {
        fontSize: 28,
        lineHeight: 42,
        // fontWeight: 'bold',
        color: '#FFFFFF',
    },
    text: {
        fontSize: 16,
        marginTop: 22,
    },
    
});

export default styles;