import React from 'react';
import { Text, Modal, Center, ScrollView } from 'native-base';
import { View, StyleSheet, Image, Platform } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from "react-redux";

import { getMyBookingRequest, cancelMyBookingRequest } from '../../apiRequests';

import { RootStackNavigationProps } from '../../common';

import Header from '../../components/Header';
import ButtonApp from '../../components/ButtonApp';
import ModalBottom from '../../components/ModalBottom';
import BookingCard from '../../components/BookingCard';

import styles from './styles';
import { Dictionary } from '@reduxjs/toolkit';


const MyBookingScreen: React.FC = () => {
  const navigation = useNavigation();
  const [bookingList, setBookingList] = React.useState<Array<any>>([]);
  const [reload, setReload] = React.useState<boolean>(true);
  const [modalVisible, setModalVisible] = React.useState<boolean>(true);
  const [bookingToBeCancel, setBookingToBeCancel] = React.useState<any>(null);
  const [modalCancelVisible, setModalCancelVisible] = React.useState<boolean>(false);

  const headerImagePath = require('../../assets/images/background/group-business-people-having-meeting-1.png');

  const accessToken = useSelector((state: any) => state.auth["accessToken"]);


  const goToFindRoomScreen = () => {
    navigation.navigate(RootStackNavigationProps.FindRoomNavigator.name);
  }

  React.useEffect(() => {
    const getMyBooking = async () => {
      const response = await getMyBookingRequest(accessToken);
      if(response.status == 200){
        setBookingList(response.data);
      }
    }
    getMyBooking();
  }, [reload])

  return (
    <View>
      <Header 
        title={"My Booking History"} 
        imageBackground={headerImagePath}
      />
      <View testID='MyBookingHistoryScreen' style={{ padding: 22 }}>
          <Text fontFamily="body" fontWeight={700} color={"secondary.100"} style={styles.labelText} >Reserved</Text>
          <ScrollView h="540">
            {bookingList.map((booking: Dictionary<any>, index: number) => {
                return (
                  <BookingCard 
                    data={booking} testID={"bookingCard-" + index} 
                    key={index}
                    onCancelButtonClick={() => {
                      setBookingToBeCancel(booking)
                      setModalCancelVisible(true)
                    }}
                  />
                )
              }
            )}
          </ScrollView>
      </View>
      <Center>
      <Modal 
          isOpen={modalCancelVisible} 
          onClose={() => setModalCancelVisible(false)} 
          avoidKeyboard 
          closeOnOverlayClick={true}
          overlayVisible={true}
          style={{ 
              marginTop: "auto",
              borderRadius: 10,
          }}
      >
          <Modal.Content style={styles.modalCancelContent}>
              <Modal.Body style={styles.modalCancelBody}>
                  <View>
                    <Text fontFamily="body" fontWeight={700} color={"secondary.100"} style={styles.modalCancelTitleText} >Cancel Booking</Text>
                  </View>
                  <View style={{marginTop: 20}}>
                    <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.modalCancelText} >
                      Booking no. {bookingToBeCancel && bookingToBeCancel["id"]}
                    </Text>
                    <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.modalCancelText} >
                      If you cancel this booking, you will lose your reservation.
                    </Text>
                    <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.modalCancelText} >
                      Are you sure you want to cancel your booking?
                    </Text>
                  </View>
                  <View style={{marginTop: 20}}>
                    <ButtonApp 
                        text="No, Keep My booking"
                        testID='CancelBooking_button_no'
                        onPress={() => {
                          setModalCancelVisible(false)
                        }}
                        styleBody={{marginBottom: 10}}
                        type={"red-white"}
                    />
                    <ButtonApp 
                        text="Yes, Cancel Booking"
                        testID='CancelBooking_button_yes'
                        onPress={() => {
                          cancelMyBookingRequest(accessToken, bookingToBeCancel["id"])
                          setReload(!reload);
                          setModalCancelVisible(false)
                            // goToFindRoomScreen()
                        }}
                        type={"red"}
                    />
                  </View>
              </Modal.Body>
          </Modal.Content>
      </Modal>
      </Center>
      

      <ModalBottom
          isOpen={modalVisible}
          footer={false}
          overlayVisible={false}
      >
        <View style={{ marginVertical: 10 }}>
          <ButtonApp 
              text="Make New Booking"
              onPress={() => {
                  setModalVisible(false)
                  goToFindRoomScreen()
              }}
              type={"green"}
          />
        </View>
      </ModalBottom>
    </View>
  )
}

export default MyBookingScreen;
