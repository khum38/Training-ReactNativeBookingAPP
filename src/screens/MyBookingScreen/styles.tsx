import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    labelText: {
        fontSize: 16,
        paddingVertical: 10,
    },
    modalCancelTitleText: {
        fontSize: 28,
        lineHeight: 42,
    },
    modalCancelText: {
        // flex: 1,
        color: "#191919",
        lineHeight: 24,
        marginTop: 20,
        // fontSize: 20,
    },
    modalCancelContent: {
        width: "100%",
        backgroundColor: "#FFFFFF",
    },
    modalCancelBody: {
        width: "100%",
    }
});

export default styles;
