import { useState, React } from 'react';
import { Input, Text, FormControl } from 'native-base';
import { View } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { useDispatch, useSelector } from "react-redux";
import { setAccessToken, setRefreshToken, setUserEmail } from '../../stores/slice/authSlice';
// import { storeStringData } from '../../utils/storage'


import { singUpRequest } from '../../apiRequests';

import DrawerApp from '../../components/Drawer';
import WelcomeLayout from '../../components/Layout/Welcome';
import BackIconButton from '../../components/BackIconButton';
import ButtonApp from '../../components/ButtonApp';

import { RootStackNavigationProps } from '../../common';
import styles from './styles';


const LoginScreen: React.FC = () => {
    const [showError, setShowError] = useState<boolean>(false); // [error, setError
    const [emailInput, setEmailInput] = useState<string>('');
    const [passwordInput, setPasswordInput] = useState<string>('');
    const dispatch = useDispatch();

    const navigation = useNavigation();

    const goToWelcomeScreen = () => {
        navigation.navigate(RootStackNavigationProps.WelcomeNavigator.name);
    };

    const goToFindRoomScreen = () => {
        navigation.navigate(RootStackNavigationProps.FindRoomNavigator.name);
    };

    const login = async () => {  
        const response = await singUpRequest(emailInput, passwordInput);

        if(response.status == 200){
            dispatch(setAccessToken(response.data["token"]));
            dispatch(setUserEmail(emailInput));

            goToFindRoomScreen();
        } else {
            setShowError(true);
        }
    };

  return (
    <WelcomeLayout 
        imageBackground={require('../../assets/images/background/pawel-chu-ULh0i2txBCY-unsplash-2.png')}
        useOverlay={true}
    >
        <BackIconButton onPress={goToWelcomeScreen} />
        <View style={styles.titleTextView}>
            <Text testID='LoginScreen' style={styles.titleText}>Hello !</Text>
            <Text style={styles.titleText} testID='TextTitle'>Welcome Back</Text>
        </View>
        <DrawerApp>
            {showError && <View style={styles.errorBox} testID='LoginErrorView'>
                <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.text}>
                    Incorrect email or password.
                </Text>
                <Text fontFamily="body" fontWeight={400} color={"secondary.100"} style={styles.text}>
                    Please try again.
                </Text>
            </View>}
            <View>
                <FormControl>
                    <FormControl.Label >
                        <Text 
                            fontFamily="body" fontWeight={400} color={"primary.100"} style={styles.labelInput}
                        >Email</Text>
                    </FormControl.Label>
                    <Input 
                        testID="login_textfield_email"
                        autoCapitalize="none"
                        size="2xl" placeholder="Email" style={styles.input} 
                        onChangeText={(value) => {setEmailInput(value)}} 
                    />
                </FormControl>

                <FormControl style={{marginTop: 10}}>
                    <FormControl.Label >
                        <Text fontFamily="body" fontWeight={400} color={"primary.100"} style={styles.labelInput}>Password</Text>
                    </FormControl.Label>
                    <Input 
                        testID="login_textfield_password"
                        size="2xl" 
                        placeholder="Password" 
                        type='password' 
                        style={styles.input} 
                        onChangeText={(value) => {setPasswordInput(value)}} 
                    />
                </FormControl>
            </View>
            <ButtonApp 
                testID="login_button_login" 
                text="Login"
                onPress={login}
                type={(emailInput && passwordInput) ? "green" : "inactive"}
            />
        </DrawerApp>
    </WelcomeLayout>
  );
};

export default LoginScreen;
