import React from 'react';
import { Text } from 'native-base';
import { View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from "react-redux";
import { setLastBooking } from '../../stores/slice/roomSlice';

import { makeBookingRoomRequest } from '../../apiRequests';

import WelcomeLayout from '../../components/Layout/Welcome';

import { RootStackNavigationProps } from '../../common';

import ModalBottom from '../../components/ModalBottom';
import ButtonApp from '../../components/ButtonApp';
import RoomSelectCard from '../../components/RoomSelectCard';

import styles from './styles';


const BookingSummaryScreen: React.FC = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const [modalVisible, setModalVisible] = React.useState<boolean>(true);

    const avilableRooms = useSelector((state: any) => state.room["avilableRooms"]);
    const selectedRoom = useSelector((state: any) => state.room["selectedRoom"]);
    const accessToken = useSelector((state: any) => state.auth["accessToken"]);

    const goToSelectRoomScreen = () => {
        navigation.navigate(RootStackNavigationProps.SelectRoomNavigator.name);
    }

    const goToMakeMyBookingScreen = () => {
        navigation.navigate(RootStackNavigationProps.MakeMyBookingNavigator.name);
    }

    const bookingRoom = async () => {
        const response = await makeBookingRoomRequest(
            accessToken, selectedRoom["id"], avilableRooms["guestNumberInput"], 
            avilableRooms["dateInput"], avilableRooms["startTimeInput"], 
            avilableRooms["endTimeInput"]
        );
        if(response.status == 200){
            dispatch(setLastBooking(response.data));
            goToMakeMyBookingScreen();
        }
    }

    return (
        <WelcomeLayout 
            imageBackground={require('../../assets/images/background/damir-kopezhanov-VM1Voswbs0A-unsplash-1.png')}
            useOverlay={false}
        >
            <View style={styles.titleTextView}>
                <Text testID='BookingSummaryScreen' fontFamily="body" fontWeight={600} style={styles.titleText}>Meeting</Text>
                <Text fontFamily="body" fontWeight={600} style={styles.titleText}>Room Booking</Text>
            </View>
            <ModalBottom
                isOpen={modalVisible}
                footer={false}
            >
                <View style={{flexDirection: "row"}}>
                    <View style={{flex: 2}}>
                        <Text fontFamily="body" fontWeight={700} color={"secondary.100"} style={styles.labelText} >Date</Text>
                        <View style={styles.informationTextBody}>
                        <Text fontFamily="body" fontWeight={400} color={"#ffffff"} style={styles.informationText} >{avilableRooms["dateInput"]}</Text>
                        </View>
                    </View>
                    <View style={{flex: 3, marginLeft: 20}}>
                        <Text fontFamily="body" fontWeight={700} color={"secondary.100"} style={styles.labelText} >Time</Text>
                        <View style={styles.informationTextBody}>
                        <Text fontFamily="body" fontWeight={400} color={"#ffffff"} style={styles.informationText} >
                            {avilableRooms["startTimeInput"] + " - " + avilableRooms["endTimeInput"]}
                        </Text>
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: 10}}>
                    <Text fontFamily="body" fontWeight={700} color={"secondary.100"} style={styles.labelText} >Selected Room</Text>
                </View>
                <RoomSelectCard 
                  testID={"RoomSelected"}
                  onPress={() => {}}
                  data={selectedRoom}
                />
                <View style={{ marginTop: 10 }}>
                    <ButtonApp 
                        text="Cancel"
                        testID='BookingSummaryScreen_button_cancel'
                        onPress={goToSelectRoomScreen}
                        type={"white"}
                        styleBody={styles.actionButtonDone}
                    />
                    <ButtonApp 
                        text="Confirm Booking"
                        testID='BookingSummaryScreen_button_confirm'
                        onPress={() => {
                            setModalVisible(false);
                            bookingRoom()
                        }}
                        type={"green"}
                        styleBody={styles.actionButtonDone}
                    />
                </View>
            </ModalBottom>
        </WelcomeLayout>
    )
}

export default BookingSummaryScreen;
