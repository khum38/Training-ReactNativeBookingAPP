import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    titleTextView: {
        top: 62,
        left: 22,
        position: 'absolute',
    },
    titleText: {
        fontSize: 28,
        lineHeight: 42,
        // fontWeight: 'bold',
        color: '#FFFFFF',
    },
    actionButtonDone: {
        flex: 1,
        height: 75,
        marginVertical: 5,
    },
    labelText: {
        fontSize: 16,
        paddingVertical: 10,
    },
    informationTextBody: {
        borderRadius: 10,
        borderWidth: 1,
        backgroundColor: "#FFFFFF",
        borderColor: "#191919",
    },
    informationText: {
        fontSize: 16,
        paddingVertical: 10,
        width: "100%",
        textAlign: "center",
        color: "#191919",
    }
});

export default styles;
