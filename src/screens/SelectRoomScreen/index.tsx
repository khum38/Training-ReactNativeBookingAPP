import React from 'react';
import { Text } from 'native-base';
import { View, StyleSheet, Image, Platform } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { useDispatch, useSelector } from "react-redux";
import { selectedRoom } from '../../stores/slice/roomSlice';

import { RootStackNavigationProps } from '../../common';

import Header from '../../components/Header';
import BackIconButton from '../../components/BackIconButton';
import RoomSelectCard from '../../components/RoomSelectCard';

import styles from './styles';
import { Dictionary } from '@reduxjs/toolkit';


const SelectRoomScreen: React.FC = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const headerImagePath = require('../../assets/images/background/pawel-chu-ULh0i2txBCY-unsplash-1.png');

  const avilableRooms = useSelector((state: any) => state.room["avilableRooms"]);

  const goToFindRoomScreen = () => {
    navigation.navigate(RootStackNavigationProps.FindRoomNavigator.name);
  }

  const goToBookingSummaryScreen = () => {
    navigation.navigate(RootStackNavigationProps.BookingSummaryNavigator.name);
  }

  return (
    <View>
      <Header 
        title={"Select Meeting Room"} 
        imageBackground={headerImagePath}
      />
      <BackIconButton onPress={goToFindRoomScreen} />
      <View style={{padding: 22}}>
        <View style={{flexDirection: "row"}}>
          <View style={{flex: 2}}>
            <Text fontFamily="body" fontWeight={700} color={"secondary.100"} style={styles.labelText} >Date</Text>
            <View style={styles.informationTextBody}>
              <Text fontFamily="body" fontWeight={400} color={"#ffffff"} style={styles.informationText} >{avilableRooms["dateInput"]}</Text>
            </View>
          </View>
          <View style={{flex: 3, marginLeft: 20}}>
            <Text fontFamily="body" fontWeight={700} color={"secondary.100"} style={styles.labelText} >Time</Text>
            <View style={styles.informationTextBody}>
              <Text fontFamily="body" fontWeight={400} color={"#ffffff"} style={styles.informationText} >
                {avilableRooms["startTimeInput"] + " - " + avilableRooms["endTimeInput"]}
              </Text>
            </View>
          </View>
        </View>
        <View style={{ marginTop: 20}}>
            <Text fontFamily="body" fontWeight={700} color={"secondary.100"} style={styles.labelText} >Available Room</Text>
            <View>
              {avilableRooms["rooms"].map((room: Dictionary<any>, index: number) => (
                <RoomSelectCard 
                  testID={"RoomSelectCard-"+index}
                  onPress={() => {
                    dispatch(selectedRoom(room));
                    goToBookingSummaryScreen();
                  }}
                  data={room} key={room["id"]} 
                />
              )
              )}
            </View>

        </View>

      </View>
      {/* <ButtonApp 
          text="Make New Booking"
          onPress={goToFindRoomScreen}
          type="green"
          styleBody={{marginTop: 10}}
      /> */}
    </View>
  )
}

export default SelectRoomScreen;
