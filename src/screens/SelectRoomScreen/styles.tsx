import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    labelText: {
        fontSize: 16,
        paddingVertical: 10,
    },
    informationTextBody: {
        borderRadius: 10,
        backgroundColor: "#5CC99B",
    },
    informationText: {
        fontSize: 16,
        paddingVertical: 10,
        width: "100%",
        textAlign: "center",
        color: "#FFFFFF",
    }
});

export default styles;