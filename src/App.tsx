import React, { Fragment } from 'react';
import { RootNavigator as RootNavigator } from './navigation'
import { Provider } from 'react-redux';
import store from './stores/reducers';
import { StatusBar, LogBox } from 'react-native';

import { NativeBaseProvider, extendTheme } from "native-base";

LogBox.ignoreLogs([ 'In React 18, SSRProvider is not necessary and is a noop. You can remove it from your app.', ])

const theme = extendTheme({
  "Open Sans": {
    100: {
      normal: "OpenSans-Light",
      italic: "OpenSans-LightItalic",
    },
    200: {
      normal: "OpenSans-Light",
      italic: "OpenSans-LightItalic",
    },
    300: {
      normal: "OpenSans-Light",
      italic: "OpenSans-LightItalic",
    },
    400: {
      normal: "OpenSans-Regular",
      italic: "OpenSans-Italic",
    },
    500: {
      normal: "OpenSans-Medium",
    },
    600: {
      normal: "OpenSans-Medium",
      italic: "OpenSans-MediumItalic",
    },
    700: {
      normal: "OpenSans-Bold",
      italic: "OpenSans-BoldItalic",
    }
  },
  fonts: {
    heading: "Open Sans",
    body: "Open Sans",
    mono: "Open Sans",
  },
  colors: {
    // Add new color
    primary: {
      100: "#706F68",
      200: "#706F68",
    },
    secondary: {
      100: "#000000",
      200: "#000000",
    }
  }
});

const App: React.FC = () => {
  return (
    <NativeBaseProvider theme={theme}>
      <Provider store={store}>
        <StatusBar translucent backgroundColor={'transparent'} barStyle={'dark-content'} />
        <RootNavigator />
      </Provider>
    </NativeBaseProvider>
  );
};

export default App;
