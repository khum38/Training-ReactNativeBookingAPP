// this lives in common folder to prevent cyclic dependencies
export type RootStackParamList = {
    // BottomTabNavigator: undefined
    WelcomeNavigator: undefined
    LoginNavigator: undefined
    CreateAccountNavigator: undefined
    FindRoomNavigator: undefined
    MyBookingNavigator: undefined
    SelectRoomNavigator: undefined
    BookingSummaryNavigator: undefined
    MakeMyBookingNavigator: undefined
}

export type BottomTabNavigatorStackParamList = {
    Home: undefined
    Test: undefined
}
export type LoginStackParamList = {
    LoginScreen: undefined
    // ForgotLoginScreen: undefined
    // PinScreen: undefined
}
