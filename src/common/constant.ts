import { RootStackParamList } from './navigationStacksParamLists'

import { createNativeStackNavigator, NativeStackNavigationOptions } from '@react-navigation/native-stack'


const Stack = createNativeStackNavigator<RootStackParamList>()

type RootStackProps = {
    [key in keyof RootStackParamList]: {
        name: keyof RootStackParamList
        options?: NativeStackNavigationOptions
    }
}

const RootStackNavigationProps: RootStackProps = {
    WelcomeNavigator: {
        name: 'WelcomeNavigator',
        options: {
            headerShown: false,
        },
    },
    LoginNavigator: {
        name: 'LoginNavigator',
        options: {
            // animation: 'slide_from_bottom',
            headerShown: false,
        },
    },
    CreateAccountNavigator: {
        name: 'CreateAccountNavigator',
        options: {
            // animation: 'slide_from_bottom',
            headerShown: false,
        },
    },
    FindRoomNavigator: {
        name: 'FindRoomNavigator',
        options: {
            animation: 'slide_from_bottom',
            headerShown: false,
        },
    },
    MyBookingNavigator: {
        name: 'MyBookingNavigator',
        options: {
            animation: 'slide_from_bottom',
            headerShown: false,
        },
    },
    SelectRoomNavigator: {
        name: 'SelectRoomNavigator',
        options: {
            animation: 'slide_from_bottom',
            headerShown: false,
        },
    },
    BookingSummaryNavigator: {
        name: 'BookingSummaryNavigator',
        options: {
            animation: 'slide_from_bottom',
            headerShown: false,
        },
    },
    MakeMyBookingNavigator: {
        name: 'MakeMyBookingNavigator',
        options: {
            animation: 'slide_from_bottom',
            headerShown: false,
        },
    },

}

export { RootStackNavigationProps, Stack }
