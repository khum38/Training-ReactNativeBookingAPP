const { Then } = require('cucumber');
const { expect, element, by } = require('detox');

Then("I should see the {string} text", async (text) => {
  await expect(element(by.text(text))).toBeVisible();
});

Then(
  'I see the text {string} with value {string}',
  async (componentId, value) => {
    await expect(element(by.id(componentId))).toHaveText(value)
  },
)

Then(
  'I see the input {string} with value {string}',
  async (componentId, value) => {
    await expect(element(by.id(componentId))).toHaveText(value)
  },
)

Then(
  'I should see the {string} component',
  async (componentId) => {
    await expect(element(by.id(componentId))).toBeVisible()
  },
)

Then(
  'I should see the {string} screen',
  async (componentId) => {
    await waitFor(element(by.id(componentId)))
      .toBeVisible()
      .withTimeout(10000)
    await expect(element(by.id(componentId))).toBeVisible()
  },
)
