const { Given } = require('cucumber');
const { expect, element, by } = require('detox');
import { sleep } from '../../support/helper';

Given('I should see the {string} element', async elementId => {
//   await sleep(10000);
  await expect(element(by.id(elementId))).toBeVisible();
});

Given('I am login as {string}', async userType => {});

Given('I launched the app', async () => {
    // do nothing​
})

Given('I am on the landing screen', async () => {
  await waitFor(element(by.id('LandingScreen')))
    .toBeVisible()
    .withTimeout(10000)
})

Given('I am on the login screen', async () => {
  await waitFor(element(by.id('LandingScreen')))
    .toBeVisible()
    .withTimeout(10000)
  await element(by.id('GoToLoginButton')).tap()
  await sleep(1000);
  await waitFor(element(by.id('LoginScreen')))
    .toBeVisible()
    .withTimeout(10000)
})

Given('I am on the create account screen', async () => {
  await waitFor(element(by.id('LandingScreen')))
    .toBeVisible()
    .withTimeout(10000)
  await element(by.id('GoToSignupButton')).tap()
  await sleep(1000);
  await waitFor(element(by.id('CreateAccountScreen')))
    .toBeVisible()
    .withTimeout(10000)
})

Given('I am on the find room screen', async () => {
  await waitFor(element(by.id('LandingScreen')))
    .toBeVisible()
    .withTimeout(10000)
  await element(by.id('GoToLoginButton')).tap()
  await sleep(1000);
  await waitFor(element(by.id('LoginScreen')))
    .toBeVisible()
    .withTimeout(10000)
  await element(by.id("login_textfield_email")).typeText("admin6@admin.com")
  await element(by.id("login_textfield_password")).typeText("admin")
  await element(by.id('login_button_login')).tap()
  await sleep(1000);
  await waitFor(element(by.id('FindRoomScreen')))
    .toBeVisible()
    .withTimeout(10000)
})