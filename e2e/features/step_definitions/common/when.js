const { When } = require('cucumber');
const { expect, element, by } = require('detox');

When('I tap on the {string} component', async (componentId) => {
    await element(by.id(componentId)).tap()
})

When('I tap {int} times on the {string} component', async (tabNumber, componentId) => {
    await element(by.id(componentId)).multiTap(tabNumber)
})

When('I fill the {string} field with {string}', async (componentId, value) => {
    await element(by.id(componentId)).typeText(value)
})
