import detox from 'detox/internals'
const { Before, BeforeAll, AfterAll, After } = require('cucumber');
const config = require('../../../package.json').detox;
// const adapter = require('./adapter');


BeforeAll({ timeout: 120 * 1000 }, async () => {
    await detox.init(config)
    await device.launchApp({
        newInstance: true,
        delete: true,
    })
})

Before(async (message) => {
    const { pickle } = message
    await device.reloadReactNative()
    await detox.onTestStart({
        title: pickle.uri,
        fullName: pickle.name,
        status: 'running',
    })
})
After(async (message) => {
    const { pickle, result } = message
    await detox.onTestDone({
        title: pickle.uri,
        fullName: pickle.name,
        status: result ? 'passed' : 'failed',
    })
})

AfterAll(async () => {    
    await detox.cleanup()
})