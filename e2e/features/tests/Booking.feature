Feature: Booking : Search Room, Booking Room, Cancel Booking

    Scenario: As a user, I land on find room page, When I click logout button, Then I should see landing page.
        Given I am on the find room screen
        When I tap on the "findingroom_guestnumber_input" component
        Then I should see the "numberOfGuest_body" component

        When I tap 3 times on the "numberOfGuest_button_plus_input" component
        When I tap on the "numberOfGuest_button_minus_input" component
        Then I see the input "findingroom_guestnumber_input" with value "3"

        When I tap on the "findingroom_done_button" component
        # When I tap on the "findingroom_starttime_input" component
        # Then I should see the "findingroom_starttime_datepicker_input" component

        When I tap on the "findingroom_Button" component
        Then I should see the "RoomSelectCard-0" component

        When I tap on the "RoomSelectCard-0" component
        Then I should see the "BookingSummaryScreen" component

        When I tap on the "BookingSummaryScreen_button_cancel" component
        Then I should see the "RoomSelectCard-0" component

        When I tap on the "RoomSelectCard-1" component
        Then I should see the "BookingSummaryScreen" component

        When I tap on the "BookingSummaryScreen_button_confirm" component
        Then I should see the "MakeMyBookingScreen" component

        When I tap on the "GoToMyBookingHistory_button" component
        Then I should see the "MyBookingHistoryScreen" component
        Then I should see the "bookingCard-0" component

        When I tap on the "bookingCard-0_CancelBooking_button" component
        Then I should see the "CancelBooking_button_no" component

        When I tap on the "CancelBooking_button_no" component
        When I tap on the "bookingCard-0_CancelBooking_button" component
        When I tap on the "CancelBooking_button_yes" component

        When I tap on the "LogoutButton" component
        Then I should see the "LandingScreen" screen
