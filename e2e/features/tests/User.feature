Feature: User : Login, Logout and create account

    Scenario: As a user, I land on landing page, When I click login button, Then I should see login page.
        Given I am on the landing screen
        When I tap on the "GoToLoginButton" component
        Then I should see the "LoginScreen" screen

    Scenario: As a user, I land on login page, When I fill the login form and I login success, Then I should see search room page.
        Given I am on the login screen
        When I fill the "login_textfield_email" field with "admin6@admin.com"
        When I fill the "login_textfield_password" field with "admin"
        When I tap on the "login_button_login" component
        Then I should see the "FindRoomScreen" screen

    Scenario: As a user, I land on login page, When I fill the login form and I login fail, Then I should see error message.
        Given I am on the login screen
        When I fill the "login_textfield_email" field with "xxx@xxx.com"
        When I fill the "login_textfield_password" field with "xxx"
        When I tap on the "login_button_login" component
        Then I should see the "LoginScreen" screen
        Then I should see the "LoginErrorView" component

    Scenario: As a user, I land on find room page, When I click logout button, Then I should see landing page.
        Given I am on the find room screen
        When I tap on the "LogoutButton" component
        Then I should see the "LandingScreen" screen

    Scenario: As a user, I land on login page, When I click create account button, Then I should see create account page.
        Given I am on the landing screen
        When I tap on the "GoToSignupButton" component
        Then I should see the "CreateAccountScreen" screen

    # Scenario: As a user, I land on create account page, When I fill the create account form and I create account success, Then I should see search room page.
    #     Given I am on the create account screen
    #     When I fill the "signup_textfield_firstname" field with "CucumberTest"
    #     When I fill the "signup_textfield_lastname" field with "Automated"
    #     When I fill the "signup_textfield_email" field with "cucumber-test@test.com"
    #     When I fill the "signup_textfield_password" field with "1234"
