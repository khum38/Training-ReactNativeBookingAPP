import { TestStepResultStatus } from '@cucumber/messages'

export const isPassedOrSkipped = (result?: TestStepResultStatus): boolean => {
    return result === TestStepResultStatus.PASSED || result === TestStepResultStatus.SKIPPED
}

export const logFinalResult = (result: boolean): void =>{
    const message = 'AfterAll: Run result is '
    if(result){
        console.log(`${message} ALL PASSED`)
    }
    else{
        console.error(`${message} NOT PASSED`)
    }
}