const report = require('multiple-cucumber-html-reporter')
const version = '1.0.0'

report.generate({
    jsonDir: 'e2e/report/ios',
    reportPath: 'e2e/report/ios',
    reportName: 'iOS E2E Test Report',
    displayDuration: 'true',
    metadata: {
        device: 'iPhone 14 Simulator',
        platform: {
            name: 'ios',
            version: '16.0',
        },
    },
    customData: {
        title: 'iOS E2E Test Report',
        data: [
            { label: 'Project', value: 'Booking App' },
            { label: 'Package', value: 'com.bookingapp' },
            { label: 'Version', value: version },
            { label: 'Description', value: 'iOS E2E Test Report for Booking App' },
            { label: 'Environment', value: 'Development' },
        ],
    },
})
