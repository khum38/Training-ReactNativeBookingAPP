const report = require('multiple-cucumber-html-reporter')
const version = '1.0.0'

report.generate({
    jsonDir: 'e2e/report/android',
    reportPath: 'e2e/report/android',
    reportName: 'Android E2E Test Report',
    displayDuration: 'true',
    metadata: {
        device: 'Android Emulator',
        platform: {
            name: 'android',
            version: '11',
        },
    },
    customData: {
        title: 'Android E2E Test Report',
        data: [
            { label: 'Project', value: 'Booking App' },
            { label: 'Package', value: 'com.bookingapp' },
            { label: 'Version', value: version },
            { label: 'Description', value: 'Android E2E Test Report for Booking App' },
            { label: 'Environment', value: 'Development' },
        ],
    },
})
